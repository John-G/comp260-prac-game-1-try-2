﻿using UnityEngine;
using System.Collections;

public class PlayerMove : MonoBehaviour {

	private BeeSpawner beeSpawner;

	void Start() {
		beeSpawner = FindObjectOfType<BeeSpawner> ();
	}
	
	public float maxSpeed = 5.0f;
	public int player = 1;
	private Vector2 direction;
	public float destroyRadius = 1.0f;

	void Update() {
		// get the input values
		if (Input.GetButton ("Fire1")) {
			// destroy nearby bees
			beeSpawner.DestroyBees(
				transform.position, destroyRadius);
		}

		if (player == 1) {
			direction.x = Input.GetAxis ("Horizontal");
			direction.y = Input.GetAxis ("Vertical");
		} else if (player == 2) {
			direction.x = Input.GetAxis ("Horizontal2");
			direction.y = Input.GetAxis ("Vertical2");
		}


		//Vector2 direction2;
		//direction2.x = Input.GetAxis("Horizontal2");
		//direction2.y = Input.GetAxis("Vertical2");


		// scale by the maxSpeed parameter
		Vector2 velocity = direction * maxSpeed;

		// move the object
		transform.Translate(velocity * Time.deltaTime);
	}
}
