﻿using UnityEngine;
using System.Collections;

public class BeeSpawner : MonoBehaviour {
	
	public int nBees = 50;
	public BeeMove beePrefab;
	public float xMin, yMin;
	public float width, height;
	private float timeSinceLast =0f;
	public float minBeePeriod = 1.0f;
	public float maxBeePeriod = 4.0f;
	private float tilNext = 0f;
	public float minSpeed;
	public float minTurnSpeed;
	public float maxSpeed;
	public float maxTurnSpeed;

	// private state
	private float speed;
	private float turnSpeed;

	private Transform target;
	private Vector2 heading;

	void Start() {
		// find the player to set the target
		PlayerMove p = FindObjectOfType<PlayerMove>();
		target = p.transform;

		// bee initially moves in random direction
		heading = Vector2.right;
		float angle = Random.value * 360;
		heading = heading.Rotate(angle);

		// set speed and turnSpeed randomly 
		speed = Mathf.Lerp(minSpeed, maxSpeed, Random.value);
		turnSpeed = Mathf.Lerp(minTurnSpeed, maxTurnSpeed,Random.value);
	}


	// Update is called once per frame
	void Update () {
		timeSinceLast += Time.deltaTime;
		if(timeSinceLast>=tilNext){
			makeBee();
		}
	}
	public void DestroyBees(Vector2 centre, float radius) {
		// destroy all bees within ‘radius’ of ‘centre’
		for (int i = 0; i < transform.childCount; i++) {
			Transform child = transform.GetChild(i);
			Vector2 v = (Vector2)child.position - centre;
			if (v.magnitude <= radius) {
				Destroy(child.gameObject);
			}
		}
	}
	private int beeNum = 0;

	void makeBee(){
		BeeMove bee = Instantiate (beePrefab);
		bee.transform.parent = transform;
		bee.gameObject.name = "Bee" + beeNum;

		float x = xMin + Random.value * width;
		float y = yMin + Random.value * height;
		bee.transform.position = new Vector2 (xMin, yMin);
		beeNum++;
		tilNext = Mathf.Lerp (minBeePeriod, maxBeePeriod, Random.value);
		timeSinceLast = 0f;
}
}