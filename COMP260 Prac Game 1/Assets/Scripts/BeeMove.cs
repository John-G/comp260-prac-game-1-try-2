﻿using UnityEngine;
using System.Collections;

public class BeeMove : MonoBehaviour {
	
		public float speed = 4.0f;        // metres per second
		public float turnSpeed = 180.0f;  // degrees per second
		public Transform player1;
		public Transform player2;
		public Vector2 heading = Vector3.right; 

	void Start () {
		//direction.magnitude;
		PlayerMove[] player = FindObjectsOfType<PlayerMove> ();
		player1 = player [0].transform;
		player2 = player [1].transform;
	}
	void Update() {
		 
		Vector2 target = player1.position - transform.position;
		Vector2 target2 = player2.position - transform.position; 
		float angle = turnSpeed * Time.deltaTime;

		Vector2 direction;
		if (target.magnitude < target2.magnitude) {
			direction = target;
		} else {
			direction = target2;
		}
		// turn left or right
		if (direction.IsOnLeft(heading)) {
			heading = heading.Rotate(angle);
		}
		else {
			heading = heading.Rotate(-angle);
		}

		transform.Translate(heading * speed * Time.deltaTime);
	}
	void OnDrawGizmos() {
		// draw heading vector in red
		Gizmos.color = Color.red;
		Gizmos.DrawRay(transform.position, heading);

		// draw target vector in yellow
		Gizmos.color = Color.yellow;
		Vector2 direction = player1.position - transform.position;
		Gizmos.DrawRay(transform.position, direction);
		Vector2 direction2 = player2.position - transform.position;
		Gizmos.DrawRay(transform.position, direction2);
	}
	public ParticleSystem explosionPrefab;

	void OnDestroy() {
		// create an explosion at the bee's current position
		ParticleSystem explosion = Instantiate(explosionPrefab);
		explosion.transform.position = transform.position;
		Destroy(explosion.gameObject, explosion.duration);
	}
		
}
