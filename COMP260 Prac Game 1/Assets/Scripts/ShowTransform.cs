﻿using UnityEngine;
using System.Collections;

public class ShowTransform : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	public Vector2 velocity; // in metres per second

	void Update() {
		// scale the velocity by the frame duration
		Vector2 move = velocity * Time.deltaTime;

		// move the object
		transform.Translate(move);
	}

}
